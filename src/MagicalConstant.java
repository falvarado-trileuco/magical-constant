import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MagicalConstant {

	public static void main(String[] args) {
		try {
			int magicConstant = run("9982");
			System.out.println(String.format("\nThe Magical Constant is  %04d", magicConstant));
		} catch (Exception e) {
			System.out.println("Error -> " + e.getMessage());
		}

	}

	private static int run(String startNumber) throws Exception {
		int number = Integer.parseInt(startNumber);

		boolean hasFourDigits = startNumber.length() == 4;
		if (!hasFourDigits) {
			throw new Exception("The number must have 4 digits");
		}

		System.out.println(String.format("Take the number %04d: \n", number));

		List<Integer> digits = getDigitsFromInt(number);

		boolean areEquals = digits.stream().distinct().count() <= 1;
		if (areEquals) {
			throw new Exception("All the digits can't be the same");
		}

		int substract = getSubtractGreatestLowest(digits);
		int result;

		while (true) {
			System.out.print(String.format("With the %04d: ", substract));

			digits = getDigitsFromInt(substract);
			result = getSubtractGreatestLowest(digits);

			if (substract == result) {
				break;
			}

			substract = result;
		}

		return substract;
	}

	private static int getSubtractGreatestLowest(List<Integer> digits) {
		int greatest = digits.stream().sorted(Collections.reverseOrder()).reduce(0,
				(total, digit) -> 10 * total + digit);

		int lowest = digits.stream().sorted().reduce(0, (total, digit) -> 10 * total + digit);

		int substract = greatest - lowest;

		System.out.print(String.format("Greatest: %04d", greatest));
		System.out.print(String.format(" / Lowest: %04d", lowest));
		System.out.println(String.format(" / Substract: %04d", substract));

		return substract;
	}

	private static List<Integer> getDigitsFromInt(int number) {
		List<Integer> digits = new ArrayList<Integer>();
		while (number > 0) {
			digits.add(number % 10);
			number /= 10;
		}
		while (digits.size() < 4) {
			digits.add(0);
		}
		return digits;
	}

}
